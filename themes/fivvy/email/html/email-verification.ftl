<html>

  <head>
      <meta charset="UTF-8">
      <title>Fivvy mailing</title>
  </head>

    <!--${kcSanitize(msg("emailTestBodyHtml",realmName))?no_esc}-->
    <!-- BEGIN MAIL -->
    <table width="700" border="0" cellpadding="0" cellspacing="0" style="margin: auto;">
        <!-- BEGIN BANNER -->
        <tr>
            <td colspan="7" style="line-height: 1; height: 382px;">
                <img src="https://cdn.getfivvy.com/keycloak/activate_account/Banner.jpg" width="700" height="382" usemap="#Map" />
            </td>
        </tr>
        <!-- END BANNER -->
        <!-- BEGIN SEPARATION -->
        <tr>
            <td height="40" colspan="7">&nbsp;</td>
        </tr>
        <!-- END SEPARATION -->
        <!-- BEGIN INFO -->
        <tr>
          <td width="40">&nbsp;</td>
          <td colspan="5">
              <p style="font-family: Helvetica, Arial; font-size: 18px; color: #505050; letter-spacing: 0; line-height: 25px; text-align: left; margin-top: 0; margin-bottom: 0; font-weight: 300;">
                Please verify your email address to access your Fivvy account by clicking:
              </p>
          </td>
          <td width="40">&nbsp;</td>
      </tr>
        <!-- END INFO -->
        <tr>
            <td height="28" colspan="7">&nbsp;</td>
        </tr>
        <!-- BEGIN BUTTON -->
        <tr>
          <td width="40">&nbsp;</td>
          <td width="110">&nbsp;</td>
          <td colspan="3">
            ${kcSanitize(msg("emailVerificationBodyHtml",link, linkExpiration, realmName, linkExpirationFormatter(linkExpiration)))?no_esc}
          </td>
          <td width="110">&nbsp;</td>
          <td width="40">&nbsp;</td>
        </tr>
        <!-- END BUTTON -->

      <tr>
        <td height="20" colspan="7">&nbsp;</td>
      </tr>

      <tr>
          <td width="40">&nbsp;</td>
          <td colspan="5">
              <p style="font-family: Helvetica, Arial; font-size: 16px; color: #505050; letter-spacing: 0; line-height: 25px; text-align: left; margin-top: 0; margin-bottom: 0;">
                For your safety, this link will expire in 12 hours. If you did not request any chance, no action is required from your end. If you have any questions, please contact Fivvy at support@getfivvy.com
              </p>
          </td>
          <td width="40">&nbsp;</td>
      </tr>

        <tr>
          <td height="50"    colspan="7">&nbsp;</td>
        </tr>
        <!-- BEGIN FOOTER -->
        <tr>
            <td height="20" colspan="7" style="background: #003087;">&nbsp;</td>
        </tr>
        <tr>
            <td width="40" style="background: #003087;">&nbsp;</td>
            <td colspan="5" align="left" valign="top" style="background: #003087;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="70" align="left" valign="middle" style="background: #003087;">
                            <a href="https://getfivvy.com/" target="_blank">
                                <img src="https://cdn.getfivvy.com/keycloak/logo_dark.png" width="70" height="53" />
                            </a>
                        </td>
                        <td width="10">&nbsp;</td>
                        <td colspan="4" align="right" valign="middle" style="background: #003087;">
                            <p style="font-family: Helvetica, Arial; font-size: 16px; color: #ffffff; letter-spacing: 0; line-height: 25px; text-align: right; margin-top: 0; margin-bottom: 0;">© Fivvy 2021 - All rights reserved</p>
                        </td>
                    </tr>
                </table>
          </td>
            <td width="40" style="background: #003087;">&nbsp;</td>
        </tr>
        <tr>
            <td height="20" colspan="7" style="background: #003087;">&nbsp;</td>
        </tr>
        <!-- END FOOTER -->



        <tr>
            <td width="40">&nbsp;</td>
            <td width="150">&nbsp;</td>
            <td width="150">&nbsp;</td>
            <td width="20">&nbsp;</td>
            <td width="150">&nbsp;</td>
            <td width="150">&nbsp;</td>
            <td width="40">&nbsp;</td>
        </tr>
    </table>
    <!-- END MAIL -->


    <map name="Map">
      <area shape="rect" coords="45,120,163,180" href="http://getfivvy.com/" target="_blank">
    </map>

  </body>

  <script>
    function testScript(variable1) {
      console.log(variable1)
    }
  </script>
</html>
