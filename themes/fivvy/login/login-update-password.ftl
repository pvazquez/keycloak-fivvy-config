<#import "template.ftl" as layout>
<#include "./resources/css/reset-password.css">

<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="styles/reset-password.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,900|Ubuntu:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;700&display=swap" rel="stylesheet">

  </head>


  <body onload="onLoad()">
    <div class="circle-one"></div>
    <div class="circle-two"></div>
    <div class="root-container">
      <div class="content-container">
        <div class="update-password-container">
          <form name="reset-password" onsubmit="return validateFormOnSubmit(event)" action="${url.loginAction}" method="post">
            <input type="text" id="username" name="username" value="${username}" autocomplete="username" readonly="readonly" style="display:none;"/>
            <input type="password" id="password" name="password" autocomplete="current-password" style="display:none;"/>
            <div class="title"> Update your password</div>

            <div class="title-line"></div>

            <label for="">New password</label>

            <div class="input-container">
              <input type="password" name="password-new" id="password-new"/>
              <img
                onclick="togglePasswordVisivility('password-new')"
                src="https://cdn.getfivvy.com/keycloak/reset_password/Eye.png"
                alt="O"
                height="21px"
                width="21px"
              >
            </div>

            <label for="">Repeat password</label>

            <div class="input-container">
              <input type="password" name="password-confirm" id="password-confirm">
              <img
                onclick="togglePasswordVisivility('password-confirm')"
                src="https://cdn.getfivvy.com/keycloak/reset_password/Eye.png"
                alt="O"
                height="21px"
                width="21px"
              >
            </div>

            <div class="error-message" id="error-message"></div>

            <div class="requirements-container">
              <div class="first-column-container">
                <div>One lowecase character.</div>
                <div>One number.</div>
                <div>8 characters minimun.</div>
              </div>

              <div class="second-column-container">
                <div>One uppercase character.</div>
                <div>One special character.</div>
              </div>
            </div>

            <button class="submit-button" type="submit">Send</button>

          </form>
        </div>

        <div class="image-container">
          <img
            src="https://cdn.getfivvy.com/keycloak/reset_password/update_your_password.svg"
            alt="Update your password!"
            height="398px"
            width="398px"
          >
        </div>
      </div>
    </div>

    <script>
      const formPassword = document.getElementById("password-new")
      const formRepeatPassword = document.getElementById("password-confirm")
      const errorMessage = document.getElementById("error-message")
      const pattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/)

      function onLoad() {
        errorMessage.style.display = "none"
      }

      function validateFormOnSubmit(event) {

        validationPassed = validateData()

        if(validationPassed) {
          errorMessage.style.border ="none"
          errorMessage.style.display = "none"
          return true
        }
        return false
      }

      function validateData() {

        const password = formPassword.value.trim()
        const repeatPassword = formRepeatPassword.value.trim()

        if(password === "") {
          errorMessage.style.border ="1px solid #E80303";
          errorMessage.style.display = "block"
          errorMessage.innerHTML = "Password must not be empty"
          return false
        }

        //check matching passwords
        if(password != repeatPassword) {
          errorMessage.style.border ="1px solid #E80303";
          errorMessage.style.display = "block"
          errorMessage.innerHTML = "Passwords does not match!"
          return false
        }

        //check for length
        if(password.length < 8) {
          errorMessage.style.border ="1px solid #E80303";
          errorMessage.style.display = "block"
          errorMessage.innerHTML = "Password must have at least 8 characters"
          return false
        }

        //check for at least one lowerCase and upperCase
        if(password.toUpperCase() == password || password.toLowerCase() == password) {
          errorMessage.style.border ="1px solid #E80303";
          errorMessage.style.display = "block"
          errorMessage.innerHTML = "Password must have at least an upper and lower case character"
          return false
        }

        //check for at least one number
        if(!/\d/.test(password)) {
          errorMessage.style.border ="1px solid #E80303";
          errorMessage.style.display = "block"
          errorMessage.innerHTML = "Password must have at least one number"
          return false
        }

        //check special characters
        if(!pattern.test(password)) {
          errorMessage.style.border ="1px solid #E80303";
          errorMessage.style.display = "block"
          errorMessage.innerHTML = "Password must have at least 1 special character"
          return false
        }

        return true
      }

      function togglePasswordVisivility(elementId) {
        console.log('id', elementId)
        const passwordInput = document.getElementById(elementId)
        console.log('inputType', passwordInput.type )

        if(passwordInput.type === "password") {
          passwordInput.type = "text"
        }
        else {
          passwordInput.type = "password"
        }
      }
    </script>
  </body>
</html>
