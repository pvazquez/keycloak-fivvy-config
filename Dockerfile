FROM quay.io/keycloak/keycloak:15.0.1
RUN rm -f /opt/jboss/keycloak/themes/fivvy.tar.gz
ADD fivvy.tar.gz  /opt/jboss/keycloak/themes/
