<#import "template.ftl" as layout>
<#include "./resources/css/confirmation-landing.css">
<#include "./resources/css/password-confirmation.css">
<#--<@layout.registrationLayout displayMessage=false; section>-->
<html>


    <#if pageRedirectUri?has_content>
      <body onLoad="sendFirebaseNotification('${(pageRedirectUri) ! ''}')">
        <div id="kc-info-message" class="info-root-container">
            <div style="height: 20px;"></div>
            <div class="second-confirmation-container">
              <div class="first-column">
                <div class="title">Thank You! Your account has been activated 😁</div>
                <div style="height: 20px;"></div>
                <a
                  class="redirect-button"
                  id="redirect-element"
                  href="https://www.getfivvy.com/"
                >
                  <img
                    class="invisible-redirect-button"
                    src="https://cdn.getfivvy.com/keycloak/btn-start.png"
                    alt="button"
                  >
                </a>
                <div style="height: 25px;"></div>
              </div>
              <div id="second-column">
                  <img
                    class="fivvy-image"
                    src="https://cdn.getfivvy.com/keycloak/cell_image.png"
                    alt="fivvy"
                    height="707px"
                    width="350px"
                  >
              </div>
            </div>
            <div class="footer-container">
              <div style="width: 202px;"></div>
              <div  class="custom-fivvy-footer">
                © Fivvy 2021 - All rights reserved - Terms and conditions
              </div>
            </div>
        </div>
      </body>


      <#elseif actionUri?has_content>
        <body onLoad="dispatchEventAndContinue(`user_clicked_verification_email`);">
          <a href="${actionUri}" style="display: none;" id="skip-landing">
            <img
              class="activate-account-button"
              src="https://cdn.getfivvy.com/keycloak/btn-activate.png"
              alt="button"
            >
          </a>
          <div class="center">
            <svg class="fijo" xmlns="http://www.w3.org/2000/svg" width="294" height="220" viewBox="0 0 294 220" fill="none">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M279.469 2.95803C273.487
              0.0361312 266.721 -0.381283 260.423 1.78749C254.126 3.95626 249.049 8.44977
              246.129 14.4391L176.377 157.505L151.108 109.566C147.999 103.669 142.785
              99.339 136.423 97.3693C130.061 95.3981 123.311 96.028 117.424 99.14C111.534 102.252
              107.208 107.47 105.24 113.84C103.272 120.21 103.9 126.963 107.009 132.857L141.279
              197.861C148.113 211.262 161.687 219.582 176.728 219.582C191.903 219.582 205.566
              211.098 212.434 197.344L290.939 36.3289C296.967 23.9624 291.82 8.99196 279.469
              2.95803ZM129.029 79.9017C133.16 79.9017 137.272 80.5271 141.25 81.7585C148.233
              83.9214 154.373 87.8118 159.222 93.0302L186.867 36.3288C192.895 23.9623 187.748
              8.99188 175.395 2.95795C163.045 -3.0745 148.089 2.07261 142.055 14.4405L107.015
              86.3159C107.92 85.7455 108.845 85.1944 109.803 84.6908C115.729 81.558 122.379
              79.9017 129.029 79.9017ZM72.3078 157.507L87.9923 125.333C88.5177 130.563
              90.0492 135.697 92.5735 140.485L115.179 183.364L108.365 197.345C101.497
              211.097 87.8336 219.582 72.6581 219.582C57.6176 219.582 44.0434 211.262 3
              7.2095 197.861L2.93928 132.857C-0.169719 126.964 -0.798939 120.21 1.16886
              113.842C3.13666 107.472 7.46255 102.252 13.3526 99.14C19.2411 96.028 25.9904
              95.4026 32.3509 97.3693C38.7113 99.3391 43.9276 103.671 47.0352 109.566L72.3078
              157.507Z" fill="#009CDF"/>
            </svg>
            <div class="circle-spinner spin"></div>
          </div>
        </body>


      <#elseif (client.baseUrl)?has_content>
        <body onload="timeoutForRedirect()">
          <a
            class="redirect-button"
            id="redirect-element"
            href="https://www.getfivvy.com"
            style="display: none"
          >
          </a>


          <div class="circle-one"></div>
          <div class="circle-two"></div>
          <div class="root-container">
            <div class="confirmation-content-container">
              <div class="password-confirmation-container">
                <div class="title">Your password has been set!</div>
                <div class="title-line"></div>
                <div class="text">You are free to login with the new credentials.</div>
                <a href="https://www.getfivvy.com">
                  <img
                    src="https://cdn.getfivvy.com/keycloak/reset_password/Info.svg"
                    alt="Go back using Fivvy!"
                  >
                </a>
              </div>

              <div class="image-container">
                <img
                  src="https://cdn.getfivvy.com/keycloak/reset_password/password_is_set.svg.svg"
                  alt="Update your password!"
                  height="571px"
                  width="503px"
                >
              </div>
            </div>
          </div>
        </body>
      </#if>


    </div>


    <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/8.6.5/firebase-app.js"></script>

    <!-- TODO: Add SDKs for Firebase products that you want to use
         https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/8.6.5/firebase-analytics.js"></script>

    <script>
      // Your web app's Firebase configuration
      // For Firebase JS SDK v7.20.0 and later, measurementId is optional
      var firebaseConfig = {
        apiKey: "AIzaSyDsPDwMZ8mMZUFKLeOacQ-lPbqhhpfrMII",
        authDomain: "fivvy-app.firebaseapp.com",
        projectId: "fivvy-app",
        storageBucket: "fivvy-app.appspot.com",
        messagingSenderId: "409563365032",
        appId: "1:409563365032:web:0eae4d6a34b980e05dcc9d",
        measurementId: "G-FZR71CZ9ED"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
      const analytics = firebase.analytics();
    </script>

  <body>

  <script>

    function sendFirebaseNotification(uri = '') {

      if(document.getElementById("redirect-element")) {
        fetch( uri, {
          "mode": 'no-cors',
          "Accept": "application/json",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "*",
          "Content-Type": "text/plain"
        })
          .then(() => console.log(uri))
          .then(() => logFirebaseEvent('verified_user_email'))
          .then(() => setTimeout(redirectAction, 4000))
          .catch(err => alert(err))
      }
    }

    function timeoutForRedirect() {
      setTimeout(redirectAction, 4000)
    }

    function dispatchEventAndContinue(event) {
      logFirebaseEvent(event)
      setTimeout(automateClick, 2000)
    }

    function automateClick() {
      document.getElementById("skip-landing").click()
    }

    function redirectAction() {
      if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        window.location.href='https://www.getfivvy.com/';
        analytics.logEvent('user_continued_with_app');
      }
      else if(document.getElementById("redirect-element")){
        document.getElementById("redirect-element").click();
        analytics.logEvent('user_continued_with_website');
      }
    }

    function logFirebaseEvent(event) {
      console.log('sending event...', event);
      analytics.logEvent(event);
    }

  </script>
<html>

<#--</@layout.registrationLayout>-->
