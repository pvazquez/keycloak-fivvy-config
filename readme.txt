


=======================================================================================================================================



Deploy de la instancia en AWS:

1- Tarzippear la carpeta fivvy dentro de /themes
    Estando en el directorio /themes, ejecutar: tar -cvzf fivvy.tar.gz fivvy

2- Reubicar ese .tar en la raiz del proyecto, a la altura del Dockerfile.

3- Correr el Dockerfile
  Estando en la raiz del proyecto, ejecutar: docker build .

4- Verificar que la imagen se haya generado correctamente.
  Ejecutar:
    docker images => copiar el ID de la imagen generada
    docker run -d [imageId] => copiar el ID del proceso
    docker exec -it [processId] /bin/bash
    cd opt/jboss/keycloak/themes => verificar que la carpeta fivvy sea la correcta

5- Para deployar por primera vez en aws, instalar aws client y configurar las credenciales
  Ejecutar
    sudo apt-get update
    sudo apt-get install awscli
    sudo aws configure => ingresar las credenciales correspondientes

6- Loguearse en el servicio AWS
  Ejecutar
    aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 384572145432.dkr.ecr.us-east-1.amazonaws.com

7- Buildear y pushear la imagen a AWS
  Ejecutar:
    docker build -t fivvy/keycloak .
    docker tag fivvy/keycloak:latest 384572145432.dkr.ecr.us-east-1.amazonaws.com/fivvy/keycloak:latest
    docker push 384572145432.dkr.ecr.us-east-1.amazonaws.com/fivvy/keycloak:latest

  Hacer un segundo tag con el Numero de version
    docker tag 384572145432.dkr.ecr.us-east-1.amazonaws.com/fivvy/keycloak:latest  384572145432.dkr.ecr.us-east-1.amazonaws.com/fivvy/keycloak:[NumeroDeVersion]
    docker push 384572145432.dkr.ecr.us-east-1.amazonaws.com/fivvy/keycloak:[NumeroDeVersion]


============================================================================================================================================
