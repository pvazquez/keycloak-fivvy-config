const formPassword = document.getElementById("password")
const formRepeatPassword = document.getElementById("confirm-password")
const errorMessage = document.getElementById("error-message")
const pattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/)

function onLoad() {
  errorMessage.style.display = "none"
}

function validateFormOnSubmit(event) {
  event.preventDefault()
  let validation = true
  validation = validateData()
  
  if(validation) {
    errorMessage.style.border ="none";
    errorMessage.style.display = "none"
  }
  return false
}

function validateData() {

  const password = formPassword.value.trim()
  const repeatPassword = formRepeatPassword.value.trim()

  if(password === "") {
    errorMessage.style.border ="1px solid #E80303";
    errorMessage.style.display = "block"
    errorMessage.innerHTML = "Password must not be empty"
    return false
  }

  //check matching passwords
  if(password != repeatPassword) {
    errorMessage.style.border ="1px solid #E80303";
    errorMessage.style.display = "block"
    errorMessage.innerHTML = "Passwords does not match!"
    return false
  }

  //check for length
  if(password.length < 8) {
    errorMessage.style.border ="1px solid #E80303";
    errorMessage.style.display = "block"
    errorMessage.innerHTML = "Password must have at least 8 characters"
    return false
  }

  //check for at least one lowerCase and upperCase
  if(password.toUpperCase() == password || password.toLowerCase() == password) {
    errorMessage.style.border ="1px solid #E80303";
    errorMessage.style.display = "block"
    errorMessage.innerHTML = "Password must have at least an upper and lower case character"
    return false
  }

  //check for at least one number
  if(!/\d/.test(password)) {
    errorMessage.style.border ="1px solid #E80303";
    errorMessage.style.display = "block"
    errorMessage.innerHTML = "Password must have at least one number"
    return false
  }

  //check special characters
  if(!pattern.test(password)) {
    errorMessage.style.border ="1px solid #E80303";
    errorMessage.style.display = "block"
    errorMessage.innerHTML = "Password must have at least 1 special character"
    return false
  }

  return true
}

function togglePasswordVisivility(elementId) {
  const passwordInput = document.getElementById(elementId)
  if(passwordInput.type === "password") {
    passwordInput.type = "text"
  }
  else {
    passwordInput.type = "password"
  }
}